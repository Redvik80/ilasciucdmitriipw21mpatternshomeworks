
interface Furniture {
    legs: "wooden" | "metal" | "wheels" | "oneMetalAndWheels";
    legsQuantity: 3 | 4 | 5 | 6;
    seat: "soft" | "solid";
    armrests: "soft" | "solid" | null;
    back: "soft" | "solid" | null;
}

class Chair implements Furniture {
    legs: "wooden" | "metal" | "wheels" | "oneMetalAndWheels" = "wooden";
    legsQuantity: 3 | 4 | 5 | 6 = 4;
    seat: "soft" | "solid" = "solid";
    armrests: "soft" | "solid" | null = null;
    back: "soft" | "solid" | null = "solid";
}

class Armchair implements Furniture {
    legs: "wooden" | "metal" | "wheels" | "oneMetalAndWheels" = "wheels";
    legsQuantity: 3 | 4 | 5 | 6 = 4;
    seat: "soft" | "solid" = "soft";
    armrests: "soft" | "solid" | null = "soft";
    back: "soft" | "solid" | null = "soft";
}

abstract class Creator {
    public abstract factoryMethod(): Furniture;
}

class ChairCreator extends Creator {
    public factoryMethod(): Furniture {
        return new Chair();
    }
}

class ArmchairCreator extends Creator {
    public factoryMethod(): Furniture {
        return new Armchair();
    }
}

let creator = new ChairCreator();
console.log(creator.factoryMethod());

creator = new ArmchairCreator();
console.log(creator.factoryMethod());