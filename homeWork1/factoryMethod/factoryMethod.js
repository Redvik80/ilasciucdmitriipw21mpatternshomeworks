"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Chair = /** @class */ (function () {
    function Chair() {
        this.legs = "wooden";
        this.legsQuantity = 4;
        this.seat = "solid";
        this.armrests = null;
        this.back = "solid";
    }
    return Chair;
}());
var Armchair = /** @class */ (function () {
    function Armchair() {
        this.legs = "wheels";
        this.legsQuantity = 4;
        this.seat = "soft";
        this.armrests = "soft";
        this.back = "soft";
    }
    return Armchair;
}());
var Creator = /** @class */ (function () {
    function Creator() {
    }
    return Creator;
}());
var ChairCreator = /** @class */ (function (_super) {
    __extends(ChairCreator, _super);
    function ChairCreator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChairCreator.prototype.factoryMethod = function () {
        return new Chair();
    };
    return ChairCreator;
}(Creator));
var ArmchairCreator = /** @class */ (function (_super) {
    __extends(ArmchairCreator, _super);
    function ArmchairCreator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ArmchairCreator.prototype.factoryMethod = function () {
        return new Armchair();
    };
    return ArmchairCreator;
}(Creator));
var creator = new ChairCreator();
console.log(creator.factoryMethod());
creator = new ArmchairCreator();
console.log(creator.factoryMethod());
