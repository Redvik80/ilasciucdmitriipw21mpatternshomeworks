export default null;

interface FurnitureBuilder {
    setLegs(value: Furniture["legs"]): void;
    setLegsQuantity(value: Furniture["legsQuantity"]): void;
    setSeat(value: Furniture["seat"]): void;
    setArmrests(value: Furniture["armrests"]): void;
    setBack(value: Furniture["back"]): void;
    readonly furniture: Furniture;
    reset(): void;
}

class FurnitureBuilderType1 implements FurnitureBuilder {
    private _furniture: Furniture;

    constructor() {
        this.reset();
    }

    public reset(): void {
        this._furniture = new Furniture();
    }

    public setLegs(value: Furniture["legs"]) {
        this._furniture.legs = value;
    }

    public setLegsQuantity(value: Furniture["legsQuantity"]): void {
        this._furniture.legsQuantity = value;
    }

    public setSeat(value: Furniture["seat"]): void {
        this._furniture.seat = value;
    }

    public setArmrests(value: Furniture["armrests"]): void {
        this._furniture.armrests = value;
    }

    public setBack(value: Furniture["back"]): void {
        this._furniture.back = value;
    }

    public get furniture(): Furniture {
        const result = this._furniture;
        this.reset();
        return result;
    }
}

class Furniture {
    legs: "wooden" | "metal" | "wheels" | "oneMetalAndWheels" = "wooden";
    legsQuantity: 3 | 4 | 5 | 6 = 4;
    seat: "soft" | "solid" = "solid";
    armrests: "soft" | "solid" | null = null;
    back: "soft" | "solid" | null = null;
}

class Director {
    private _builder: FurnitureBuilder;

    public get builder() {
        return this._builder;
    }

    public set builder(builder: FurnitureBuilder) {
        this._builder = builder;
    }

    buildStool() {
        this.builder.setLegs("wooden");
        this.builder.setLegsQuantity(4);
        this.builder.setSeat("solid");
        this.builder.setArmrests(null);
        this.builder.setBack(null);
    }

    buildChair() {
        this.builder.setLegs("wooden");
        this.builder.setLegsQuantity(4);
        this.builder.setSeat("solid");
        this.builder.setArmrests(null);
        this.builder.setBack("solid");
    }

    buildArmchair() {
        this.builder.setLegs("wheels");
        this.builder.setLegsQuantity(4);
        this.builder.setSeat("soft");
        this.builder.setArmrests("soft");
        this.builder.setBack("soft");
    }

    buildPcArmchair() {
        this.builder.setLegs("oneMetalAndWheels");
        this.builder.setLegsQuantity(5);
        this.builder.setSeat("soft");
        this.builder.setArmrests("solid");
        this.builder.setBack("soft");
    }
}

const director = new Director();
const builder = new FurnitureBuilderType1();
director.builder = builder;

director.buildChair();
console.log(builder.furniture);

director.buildPcArmchair();
console.log(builder.furniture);