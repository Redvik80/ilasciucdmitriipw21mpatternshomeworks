"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = null;
var FurnitureBuilderType1 = /** @class */ (function () {
    function FurnitureBuilderType1() {
        this.reset();
    }
    FurnitureBuilderType1.prototype.reset = function () {
        this._furniture = new Furniture();
    };
    FurnitureBuilderType1.prototype.setLegs = function (value) {
        this._furniture.legs = value;
    };
    FurnitureBuilderType1.prototype.setLegsQuantity = function (value) {
        this._furniture.legsQuantity = value;
    };
    FurnitureBuilderType1.prototype.setSeat = function (value) {
        this._furniture.seat = value;
    };
    FurnitureBuilderType1.prototype.setArmrests = function (value) {
        this._furniture.armrests = value;
    };
    FurnitureBuilderType1.prototype.setBack = function (value) {
        this._furniture.back = value;
    };
    Object.defineProperty(FurnitureBuilderType1.prototype, "furniture", {
        get: function () {
            var result = this._furniture;
            this.reset();
            return result;
        },
        enumerable: false,
        configurable: true
    });
    return FurnitureBuilderType1;
}());
var Furniture = /** @class */ (function () {
    function Furniture() {
        this.legs = "wooden";
        this.legsQuantity = 4;
        this.seat = "solid";
        this.armrests = null;
        this.back = null;
    }
    return Furniture;
}());
var Director = /** @class */ (function () {
    function Director() {
    }
    Object.defineProperty(Director.prototype, "builder", {
        get: function () {
            return this._builder;
        },
        set: function (builder) {
            this._builder = builder;
        },
        enumerable: false,
        configurable: true
    });
    Director.prototype.buildStool = function () {
        this.builder.setLegs("wooden");
        this.builder.setLegsQuantity(4);
        this.builder.setSeat("solid");
        this.builder.setArmrests(null);
        this.builder.setBack(null);
    };
    Director.prototype.buildChair = function () {
        this.builder.setLegs("wooden");
        this.builder.setLegsQuantity(4);
        this.builder.setSeat("solid");
        this.builder.setArmrests(null);
        this.builder.setBack("solid");
    };
    Director.prototype.buildArmchair = function () {
        this.builder.setLegs("wheels");
        this.builder.setLegsQuantity(4);
        this.builder.setSeat("soft");
        this.builder.setArmrests("soft");
        this.builder.setBack("soft");
    };
    Director.prototype.buildPcArmchair = function () {
        this.builder.setLegs("oneMetalAndWheels");
        this.builder.setLegsQuantity(5);
        this.builder.setSeat("soft");
        this.builder.setArmrests("solid");
        this.builder.setBack("soft");
    };
    return Director;
}());
var director = new Director();
var builder = new FurnitureBuilderType1();
director.builder = builder;
director.buildChair();
console.log(builder.furniture);
director.buildPcArmchair();
console.log(builder.furniture);
