class DataService {
    private static instance: DataService;
    data: number;

    private constructor() {
        this.data = Math.random();
    }

    static getInstance() {
        if (!DataService.instance) {
            DataService.instance = new DataService();
        }
        return DataService.instance;
    }
}

// new DataService();

const service1 = DataService.getInstance();
const service2 = DataService.getInstance();
console.log(service1 === service2);
console.log(service1.data === service2.data);