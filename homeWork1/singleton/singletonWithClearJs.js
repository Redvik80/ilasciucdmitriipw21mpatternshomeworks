class DataService {
    static #instance;

    static #secretKey = Math.random();

    /**Никаких new. Конструктор приватный. Используй метод getInstance для создания новых экзэмпляров класса. */
    constructor(
        /**Название параметра говорит само за себя. */
        doNotTouchThisParam = null
    ) {
        if (doNotTouchThisParam === DataService.#secretKey) {
            this.data = Math.random();
        } else {
            throw new Error("Ну там же написано, используй метод getInstance для создания новых экземпляров класса.")
        }
    }

    static getInstance() {
        if (!DataService.#instance) {
            DataService.#instance = new DataService(DataService.#secretKey);
        }
        return DataService.#instance;
    }
}

// new DataService();

const service1 = DataService.getInstance();
const service2 = DataService.getInstance();
console.log(service1 === service2);
console.log(service1.data === service2.data);