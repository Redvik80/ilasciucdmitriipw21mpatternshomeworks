"use strict";
var DataService = /** @class */ (function () {
    function DataService() {
        this.data = Math.random();
    }
    DataService.getInstance = function () {
        if (!DataService.instance) {
            DataService.instance = new DataService();
        }
        return DataService.instance;
    };
    return DataService;
}());
// new DataService();
var service1 = DataService.getInstance();
var service2 = DataService.getInstance();
console.log(service1 === service2);
console.log(service1.data === service2.data);
