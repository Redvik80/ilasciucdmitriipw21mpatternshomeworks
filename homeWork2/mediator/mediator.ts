class Input {
    onInput(value: string) { }
}

class App {
    passInput1 = new Input;
    passInput2 = new Input;

    password: string;
    passwordConfirm: string;

    constructor() {
        this.passInput1.onInput = (value: string) => {
            this.password = value;
            this.checkEquality();
        }
        this.passInput2.onInput = (value: string) => {
            this.passwordConfirm = value;
            this.checkEquality();
        }
    }

    checkEquality() {
        if (this.password !== this.passwordConfirm) console.log("Пароли не равны.");
        else console.log("Пароли совпадают.")
    }
}

const app = new App;
app.passInput1.onInput("001");
app.passInput2.onInput("001");