"use strict";
class Input {
    onInput(value) { }
}
class App {
    constructor() {
        this.passInput1 = new Input;
        this.passInput2 = new Input;
        this.passInput1.onInput = (value) => {
            this.password = value;
            this.checkEquality();
        };
        this.passInput2.onInput = (value) => {
            this.passwordConfirm = value;
            this.checkEquality();
        };
    }
    checkEquality() {
        if (this.password !== this.passwordConfirm)
            console.log("Пароли не равны.");
        else
            console.log("Пароли совпадают.");
    }
}
const app = new App;
app.passInput1.onInput("001");
app.passInput2.onInput("001");
//# sourceMappingURL=mediator.js.map