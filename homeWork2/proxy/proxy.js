"use strict";
class CalculatorObj {
    addition(x, y) {
        return x + y;
    }
}
class CalculatorLogProxy {
    constructor(calculator) {
        this.calculator = calculator;
    }
    addition(x, y) {
        const sum = this.calculator.addition(x, y);
        console.log(`${x} + ${y} = ${sum}`);
        return sum;
    }
}
const calculator = new CalculatorObj();
const proxy = new CalculatorLogProxy(calculator);
console.log(calculator.addition(2, 2) + " = " + proxy.addition(2, 2));
//# sourceMappingURL=proxy.js.map