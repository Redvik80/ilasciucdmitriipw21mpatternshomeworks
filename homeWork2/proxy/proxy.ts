interface Calculator {
    addition(x: number, y: number): number;
}

class CalculatorObj implements Calculator {
    addition(x: number, y: number) {
        return x + y;
    }
}


class CalculatorLogProxy implements Calculator {
    private calculator: Calculator;

    constructor(calculator: Calculator) {
        this.calculator = calculator;
    }

    addition(x: number, y: number) {
        const sum = this.calculator.addition(x, y);
        console.log(`${x} + ${y} = ${sum}`);
        return sum;
    }
}


const calculator = new CalculatorObj();

const proxy = new CalculatorLogProxy(calculator);

console.log(calculator.addition(2, 2) + " = " + proxy.addition(2, 2));


